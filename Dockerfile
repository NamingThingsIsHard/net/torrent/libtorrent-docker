# Common base image
FROM debian:bullseye as base

RUN apt-get update -qq \
      && apt-get install -yqq  \
            --no-install-recommends \
            python3.9-minimal \
            libboost-python1.74.0 \
      && rm -rf /var/cache/apt /var/lib/apt/lists/*

# Image to build
FROM base as builder
RUN apt-get update -qq \
      && apt-get install -yqq \
            cmake \
            build-essential \
            libboost-dev \
            libboost-python-dev \
            libboost-system-dev \
            libboost-tools-dev \
            libssl-dev \
            ninja-build \
      && rm -rf /var/cache/apt /var/lib/apt/lists/*

WORKDIR /libtorrent
COPY libtorrent .

RUN mkdir -p build \
    && cd build \
    && cmake \
      -DCMAKE_BUILD_TYPE=Release \
      -DCMAKE_CXX_STANDARD=14 \
      -Dpython-bindings=ON \
      -Dwebtorrent=ON \
      -G Ninja .. \
    && ninja \
    && ls -lh && find

# The actual image that contains the build outputs
FROM base as final
COPY --from=builder /libtorrent/build/libtorrent-rasterbar.so* /usr/local/lib/
COPY --from=builder /libtorrent/build/bindings/python/libtorrent.*.so /usr/lib/python3.9/
