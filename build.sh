set -xeuo pipefail

GIT_REF="${1:-master}"
if [[ -f "$TRIGGER_PAYLOAD" ]] ; then
  ref_name=$(grep ref_name "$TRIGGER_PAYLOAD" | cut -d '=' -f 2)
  GIT_REF="${ref_name:-$GIT_REF}"
fi

(
  [[ ! -d libtorrent ]] && git clone \
      --recurse-submodules \
      https://gitlab.com/NamingThingsIsHard/net/torrent/libtorrent.git
  cd libtorrent
  git checkout "$GIT_REF"
)

if [ "${GIT_REF}" = "master" ]; then
    TAG="latest"
fi

docker build -t "libtorrent:${TAG}" .
