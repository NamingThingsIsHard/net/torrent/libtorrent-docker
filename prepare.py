"""
libtorrent-docker
Copyright (C) 2021 LoveIsGrief

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import copy
import json
import logging
import os
import subprocess
from pathlib import Path
from typing import Any

LIBTORRENT_GIT_URI = "https://gitlab.com/NamingThingsIsHard/net/torrent/libtorrent.git"


def main():
    env = os.environ

    payload = TriggerPayload()
    payload.load()

    # The reference of libtorrent that we will checkout
    libtorrent_ref = payload.var("GIT_REF", env.get("GIT_REF", "master"))
    project_path = Path(env["CI_PROJECT_DIR"])
    checkout_dir = project_path / "libtorrent"
    checkout_libtorrent(libtorrent_ref, checkout_dir)

    # Write all the variables we want to export
    variables = payload.vars
    variables["GIT_REF"] = libtorrent_ref
    vars_sh = export_sh_vars(variables, project_path)
    logging.info("Exported payload + extra variables to %s", vars_sh)


def checkout_libtorrent(reference: str, target_dir: Path):
    logging.info("Checking out libtorrent %s", reference)
    subprocess.check_call(["git", "clone", "--recurse-submodules", LIBTORRENT_GIT_URI, target_dir])
    subprocess.check_call(["git", "-C", target_dir, "checkout", reference])


def export_sh_vars(variables, folder: Path) -> Path:
    """
    Exports the TRIGGER_PAYLOAD variables to individual files and a bash "source"able script to load them

    :param folder: Where to put the file in
    :return: The path to "source"able script
    """
    exported = []
    folder.mkdir(parents=True, exist_ok=True)
    for key, val in variables.items():
        var_file = folder / f"var_{key}"
        var_file.write_text(val)
        logging.info("Wrote var %s to %s", key, var_file)

        exported.append(f'{key}=$(cat "{var_file.name}")')
    vars_sh = folder / "vars.sh"
    vars_sh.write_text("\n".join(exported))
    return vars_sh


class TriggerPayload:
    """
    Wrapper around TRIGGIER_PAYLOAD var of GitlabCI

    https://docs.gitlab.com/ee/ci/variables/predefined_variables.html

    It's present on pipelines triggered by an API call
    https://docs.gitlab.com/ee/ci/triggers/index.html#use-a-webhook-payload
    """

    def __init__(self):
        self.payload = {}

    def load(self):
        if payload_path_str := os.environ.get("TRIGGER_PAYLOAD"):
            self.payload = json.loads(Path(payload_path_str).read_text())

    def val(self, name: str, default: Any = None):
        return self.payload.get(name, default)

    @property
    def id(self):
        return self.val("id")

    @property
    def ref(self):
        return self.val("ref")

    @property
    def vars(self):
        return copy.deepcopy(self.payload.get("variables", {}))

    def var(self, name: str, default: Any = None):
        return self.vars.get(name, default)


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)
    main()
