# libtorrent-docker

Provide docker images of  libtorrent

The image has libtorrent compiled and ready for use in C++ as well as [Python](https://libtorrent.org/python_binding.html#example).

## How to use

Pull it from the repository

```shell
docker pull registry.gitlab.com/namingthingsishard/net/torrent/libtorrent-docker
```

## How it works

The original [libtorrent repo](https://github.com/arvidn/libtorrent) is mirrored 
 from github to [gitlab](https://gitlab.com/NamingThingsIsHard/net/torrent/libtorrent/).
That [triggers a webhook](https://docs.gitlab.com/ee/ci/triggers/#use-a-webhook) which triggers the pipeline in this repo.
